/**
 * Module dependencies.
 */

const { Library, ObjClasses } = require('corefwnode');

const express = require('express');
//import express from 'express';
const http = require('http');
//import http from 'http';
const path = require('path');
//import path from 'path';
const url = require('url');
//import url from 'url';
const config = require('config');
//import config from 'config';
const bodyParser = require('body-parser');
//import bodyParser from 'body-parser';
const cookieParser = require('cookie-parser');
//import cookieParser from 'cookie-parser';
const useragent = require('express-useragent');
//import useragent from 'express-useragent';
const cors = require('cors');
//import cors from 'cors';

class App extends Library
{
    constructor()
    {
        super(config);

        const me = this;

        me.initialize();
    }

    initialize()
    {
        const me = this;

        console.log('Client started');

        const app = express();

        // app.set('port', process.env.PORT || 8000);
        app.set('views', `${__dirname}/views`);
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(cookieParser());
        app.use(useragent.express());
        if (process.env.NODE_ENV === 'dev')
        {
            app.use(cors({ credentials: true, origin: true }));
        }
        app.use(express.static(path.join(__dirname, 'static')));

        app.vars = this.vars;

        /*
        var privateKey  = fs.readFileSync('sslcert/server.key', 'utf8');
        var certificate = fs.readFileSync('sslcert/server.crt', 'utf8');

        var credentials = {key: privateKey, cert: certificate};

        https.createServer(credentials, app).listen(443, "0.0.0.0", function()
        {
            console.log("Express server listening on port " + app.get('port'));
        });
        */

        http.createServer(app).listen(8000, '0.0.0.0', () =>
        {
            console.log(`Express server listening on port ${app.get('port')}`);
        });

        app.get('/', (req, res) =>
        {
            res.render('index', { title: 'WiAutomate' });
        });

        const objects = require('./objects')();

        app.all('/*', (req, res, next) =>
        {
            // res.header("Access-Control-Allow-Origin", "*");
            // res.header("Access-Control-Allow-Headers", "X-Requested-With");
            // res.header("Access-Control-Allow-Credentials", "true");

            let lang = false;
            try
            {
                var urlParams = url.parse(req.url).pathname.split('/');

                var index = 1;
                if (urlParams[index].length == 2)
                {
                    lang = urlParams[index].toLowerCase();
                    index = 2;
                }
            }
            catch (err)
            {
                console.log(err);
            }

            if (typeof req.url !== 'undefined')
            {
                console.log('----------------------------------------');
                console.log(`Requesting: ${req.url}`);
            }
            const session = new objects.session.class(undefined, req, res);
            session.initialize(lang).then(async () =>
            {
                if (typeof req.url !== 'undefined')
                {
                    try
                    {
                        let className;
                        let method;
                        if (typeof objects[urlParams[index].toLowerCase()] !== 'undefined')
                        {
                            className = objects[urlParams[index].toLowerCase()].class;
                            method = objects[urlParams[index].toLowerCase()].methods[urlParams[(index + 1)].toLowerCase()];
                        }
                        else
                        {
                            if (typeof ObjClasses[urlParams[index].toLowerCase()] === 'undefined')
                            {
                                throw (`Cannot find class: ${urlParams[index]}`);
                            }
                            className = ObjClasses[urlParams[index].toLowerCase()].class;
                            method = ObjClasses[urlParams[index].toLowerCase()].methods[urlParams[(index + 1)].toLowerCase()];
                        }

                        const instance = new className(session);
                        let params = {};
                        if (req.method == 'GET')
                        {
                            params = req.query;
                        }
                        else
                        {
                            params = req.body;
                        }

                        await instance[method](params).then((val) =>
                        {
                            if (typeof val !== 'undefined')
                            {
                                res.header('Content-type', 'application/json');
                                res.end(JSON.stringify(val));
                            }
                        });
                    }
                    catch (e)
                    {
                        console.log(e);
                        res.header('Content-type', 'application/json');
                        if (typeof e.error !== 'undefined')
                        {
                            res.end(JSON.stringify(e));
                        }
                        else if (typeof e.sqlError !== 'undefined')
                        {
                            const errorControl = new me.ErrorControl(session);
                  res.end(JSON.stringify(errorControl.sqlError(e.sqlError.code, e.sqlError.sqlMessage)));
                        }
                        else if (e.toString() == 'TypeError: instance[method] is not a function')
                        {
                            res.end(JSON.stringify({ error: true, notice: `Method ${urlParams[2]} in class ${urlParams[1]} does not exist` }));
                        }
                        else
                        {
                            res.end(JSON.stringify({ error: true, notice: e.toString() }));
                        }
                    }
                }
            }).catch((err) =>
            {
                console.log(err);
            });
        });
    }
}

new App();
