module.exports =
  {
  	mysql: {
  		host: 'localhost',
  		username: 'dorianC',
  		password: 'Pa$$w0rd',
  		port: '3306',
  		dbname: 'retrotape',
      connectionLimit: 100
  	},
    clientSideLocation: '../retrotape_clientside',
    serverSideLocation: '../retrotape_serverside',
  	cookieName: 'RetroTape',
  	cookieLenght: 57600000,
  	salt: '34b470a9304545f7f7013c632af8834c',
  	siteUrl: 'http://www.retrotape.loc',
  	siteName: 'RetroTape',
  	siteEmail: 'info@coreaplikacije.hr'
  };