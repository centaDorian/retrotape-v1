const fs = require('fs');

module.exports = () =>
{
    const classes = {};
    fs.readdirSync(__dirname).forEach((file) =>
    {
        if (file === 'index.js')
        {
            return;
        }
        const name = file.substr(0, file.indexOf('.'));

        // eslint-disable-next-line import/no-dynamic-require, global-require
        const classDat = require(`./${name}`);
        let obj = classDat.prototype;
        let methods = [];
        do
        {
            methods = methods.concat(Object.getOwnPropertyNames(obj));
            obj = Object.getPrototypeOf(obj);
        }
        while (obj);

        const methodsOut = {};
        for (let i = 0; i < methods.length; i += 1)
        {
            const method = methods[i];
            methodsOut[method.toLowerCase()] = method;
        }

        classes[name.toLowerCase()] = {
            origName: name,
            class: classDat,
            methods: methodsOut,
        };
    });

    return classes;
};
