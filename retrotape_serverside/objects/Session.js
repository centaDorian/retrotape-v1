const { ObjClasses } = require('corefwnode');

/**
 * Translator object
 *
 * @version $Id$
 * @copyright 2008
 */
module.exports = class Session extends ObjClasses.session.class
{
    returnJsVars()
    {
        const me = this;

        return new Promise((resolve) =>
        {
            resolve({
                groupId: me.session.groupId,
                sessionId: me.session.sessionId,
                username: me.session.username,
                acl: me.lib.acl[me.session.groupId],
                langId: me.session.langId,
            });
        });
    }
};
