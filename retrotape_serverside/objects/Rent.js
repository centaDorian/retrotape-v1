
const md5 = require('md5');
const moment = require('moment');
const { CfwObject } = require('corefwnode');

module.exports = class Rent extends CfwObject
{
    constructor(session)
    {
        const proxy = super(session);

        return proxy;
    }

    // eslint-disable-next-line class-methods-use-this
    tableConf()
    {
        return {
            tableName: 'posudba',
            id: {
                idposudba: {
                    fieldType: 'int',
                    maxlength: 20,
                    req: 0,
                },
            },
            ownerField: 'idposudba',
            companyField: 'companyId',
            fields: {
                userId: {
                    fieldType: 'int',
                    maxlength: 20,
                    minlength: 1,
                    req: 0,
                },
                filmId: {
                    fieldType: 'int',
                    maxlength: 20,
                    req: 1,
                },
                datumPosudbe: {
                    fieldType: 'dateTime',
                    req: 1,
                },
                datumPovratka: {
                    fieldType: 'dateTime',
                    req: 1,
                },
                cijenaPosudbe: {
                    fieldType: 'decimal',
                    req: 1,
                },
            },
        };
    }

    async selectRow(param)
    {
        const me = this;

        let data = [];
        let dir = 'DESC';

        let sort = `u.${Object.keys(me.idField)[0]}`;

        if (typeof param.sort !== 'undefined')
        {
            sort = `u.${me.db.escape(param.sort)}`;
        }

        if (typeof param.dir !== 'undefined')
        {
            dir = me.db.escape(param.dir);
        }

        let limit = '';
        if (typeof param.start !== 'undefined' && typeof param.limit !== 'undefined')
        {
            const start = me.db.escape(parseInt(param.start, 10));
            const end = me.db.escape(parseInt(param.limit, 10));
            limit = ` LIMIT ${start}, ${end} `;
        }

        // let whereSearch = '';
        // const params = {};
        // if (typeof param.query !== 'undefined' && param.query !== '')
        // {
        //     const searchString = [];
        //     for (const key in me.tableConfig.fields)
        //     {
        //         if (me.tableConfig.fields[key])
        //         {
        //             const value = me.tableConfig.fields[key];
        //             if (value.fieldType === 'date' || value.fieldType === 'dateTime')
        //             {
        //                 continue;
        //             }
        //             searchString.push(` u.${key} LIKE :${key} `);
        //             params[key] = `%${param.query}%`;
        //         }
        //     }
        //     if (searchString.length > 0)
        //     {
        //         whereSearch = ` AND (${searchString.join(' OR ')}) `;
        //     }
        // }

        // if (typeof param.conditions !== 'undefined' && param.conditions !== '')
        // {
        //     for (const key in param.conditions)
        //     {
        //         if (param.conditions[key])
        //         {
        //             const value = me.db.escape(param.conditions[key]);
        //             whereSearch += ` AND u.${value} = :${value} `;
        //             params[value] = param.bind[key];
        //         }
        //     }
        // }

        // if (typeof param.filter !== 'undefined' && param.filter !== '')
        // {
        //     const filters = JSON.parse(param.filter);
        //     for (const key in filters)
        //     {
        //         if (filters[key])
        //         {
        //             const value = filters[key];
        //             const keySec = me.db.escape(value.property);
        //             whereSearch += ` AND u.${keySec} LIKE :${keySec} `;
        //             params[keySec] = `%${value.value}%`;
        //         }
        //     }
        // }

        // let ownerWhere = '';
        // if (me.ownerActionGlob === true)
        // {
        //     ownerWhere = ` AND u.${me.tableConfig.ownerField}=:sessionUserId `;
        //     params.sessionUserId = me.session.userId;
        // }

        // if (me.companyActionGlob === true)
        // {
        //     ownerWhere += ` AND u.${me.tableConfig.companyField}=:sessionCompanyId `;
        //     params.sessionCompanyId = me.session.companyId;
        // }

        // let results = await me[me.dbName].query(`SELECT 
        //     ud.*, u.*, uc.companyName 
        //     FROM \`${me.tableName}\` u 
        //     LEFT JOIN users_data ud ON ud.userId=u.userId 
        //     LEFT JOIN user_companies uc ON uc.companyId=u.companyId 
        //     WHERE 1=1 ${ownerWhere} ${whereSearch} 
        //     ORDER BY ${sort} ${dir} ${limit}`, params);

            let results = await me[me.dbName].query(`SELECT 
            posudba.*,user.usersName, film.filmName
            FROM \`${me.tableName}\` posudba 
            LEFT JOIN user ON user.iduser = posudba.userId
            LEFT JOIN film ON film.idfilm = posudba.filmId`);

        for (const result of results)
        {
            data.push(result);
        }

        if (typeof param.initialId !== 'undefined')
        {
            data = await me.findInitialId(data, param.initialId);
        }

        results = await me[me.dbName].query(`SELECT COUNT(*) AS cnt 
        FROM \`${me.tableName}\` u `);

        const numRows = results[0].cnt;

        return { root: data, totalCount: numRows };
    }

    async insertRow(param)
    {
        // const me = this;
        // try
        // {
        //     me.db = await me.db.begin(me.session);
        //     me.username = param.username;
        //     me.password = await me.session.getHashedPassword(param.password);
        //     me.userEmail = param.userEmail;
        //     me.dateEntered = moment.utc().format('YYYY-MM-DD HH:mm:ss');
        //     me.groupId = 2;
        //     me.status = 'active';
        //     me.userId = false;
        //     const val = await me.insert();

        //     param.id = { userId: val.lastId };
        //     const userData = new UsersData(me.session);
        //     userData.db = me.db;
        //     await userData.insertRow(param);
        //     await me.db.commit();
        //     return { error: false, notice: 'success' };
        // }
        // catch (e)
        // {
        //     me.db.rollback();
        //     throw (e);
        // }
        const me = this;
        try
        {
            me.db = await me.db.begin(me.session);
            me.datumPosudbe = param.datumPosudbe;
            me.datumPovratka = param.datumPovratka;
            me.userId = param.userId;
            me.filmId = param.filmId;
            me.cijenaPosudbe = param.cijenaPosudbe;
            const val = await super.insert();

            // param.id = { idfilm: val.lastId };
            // const films = new Films(me.session);
            // films.db = me.db;
            await me.db.commit();
            return { error: false, notice: 'success' };
        }
        catch (e)
        {
            me.db.rollback();
            throw (e);
        }

    }
    async updateRow(param)
    {
        const me = this;
        try
        {
            me.db = await me.db.begin(me.session);
            if (param.password && param.password !== '')
            {
                param.password = await me.session.getHashedPassword(param.password);
            }
            else
            {
                delete (param.password);
            }

            for (const key in param)
            {
                if (param[key] !== undefined)
                {
                    me[key] = param[key];
                }
            }
            await super.update();
            // const userData = new UsersData(me.session);
            // userData.id = param.id;
            // try
            // {
            //     await userData.select();
            //     await userData.updateRow(param);
            // }
            // catch (error)
            // {
            //     const secParams = { ...param };
            //     secParams.id = { userId: param.id };
            //     await userData.insertRow(secParams);
            // }

            await me.db.commit();
            return { error: false, notice: 'success' };
        }
        catch (e)
        {
            me.db.rollback();
            throw (e);
        }
    }

    async registerUser(param)
    {
        const me = this;
        try
        {
            me.username = param.username;
            me.password = await me.session.getHashedPassword(param.password);
            me.userEmail = param.email;
            me.groupId = '2';
            me.dateEntered = moment.utc().format('YYYY-MM-DD HH:mm:ss');
            me.status = 'inactive';

            const userdata = new UsersData(me.session);
            userdata.name = param.name;
            userdata.surname = param.surname;
            userdata.address = param.address;
            userdata.city = param.city;
            userdata.postalCode = param.postalCode;
            userdata.phone = param.phone;
            userdata.countryId = param.countryId;

            me.db = await me.db.begin(me.session);

            const val = await me.insert();

            userdata.id = { userId: val.lastId };
            await userdata.insert();

            /* TODO
            const subject = `${me.config.siteName} ${me.translate('USERACCOUNTACTIVATION')}`;
            const mailTable = `${me.translate('USERACTIVATIONPREMAILTEXT')}<br />
            <a href="${me.config.siteUrl}/register.html?activate=${me.password}">
            ${me.config.siteUrl}/register.html?activate=${me.password}</a>`;
            const message = `From: ${me.config.siteEmail},
             Message: ${me.translate('USERACCOUNTACTIVATION')}<br />${mailTable}`;
            const headers = me.config.siteEmail;
            const mail = new MailMe();
            mail.sendMail(me.userEmail, subject, headers, me.config.siteName, message);
            */

            me.db.commit();
            return val;
        }
        catch (e)
        {
            me.db = me.db.rollback();
            throw (e);
        }
    }

    async activateUser(pass)
    {
        const me = this;

        await me.db.query('UPDATE users SET `status`=\'active\' WHERE password=?', [pass]);
        return { error: false, notice: 'success' };
    }

    async editPassword(param)
    {
        const me = this;

        me.password = await me.session.getHashedPassword(param.password);
        me.id = param.id;
        const val = await me.update();
        return val;
    }

    async changePassword(param)
    {
        const me = this;

        const results = await me.db.query('SELECT * FROM users WHERE userId=?', [me.session.userId]);

        const row = results[0];
        if (row.password !== await me.session.getHashedPassword(param.oldPassword))
        {
            throw (ErrorCodes.error('old_password_invalid'));
        }

        if (param.password !== param.password2)
        {
            throw (ErrorCodes.error('passwords_do_not_match'));
        }

        me.password = await me.session.getHashedPassword(param.password);
        me.id = me.session.userId;
        const val = await me.update();
        return val;
    }

    async resetPassword(param)
    {
        const me = this;

        if (md5(param.verify) !== param.code)
        {
            throw (ErrorCodes.error('verification_code_entered_wrong'));
        }
        const results = await me.db.query(`SELECT userId, userEmail 
        FROM users 
        WHERE userEmail=:query OR username=:query`, { query: param.string });

        if (typeof results[0] !== 'undefined')
        {
            const row = results[0];
            /*
            const subject = `${me.config.siteName} ${me.translate('PASSWORDRESET')}`;
            const mailTable = `${me.translate('PASSWORDRESETTEXT')}<br />
            <a href="${me.config.siteUrl}/register-changepassword-${md5(param.verify)}/register.html">
            ${me.config.siteUrl}/register-changepassword-${md5(param.verify)}/register.html</a>`;
            const message = `From: ${me.config.siteEmail}, Message: ${me.translate('PASSWORDRESET')}<br />${mailTable}`;
            const headers = me.config.siteEmail;
            const mail = new MailMe();
            await mail.sendMail(row.userEmail, subject, headers, me.config.siteName, message);
            */

            const expireDate = moment().add(10800, 'seconds');

            await me.db.query(
                `INSERT INTO reset_password (userId, verifyCode, expireDate) 
                    VALUES(:userId, :verifyCode, :expireDate)
                    ON DUPLICATE KEY UPDATE verifyCode=:verifyCode, expireDate=:expireDate`,
                {
                    userId: row.userId,
                    verifyCode: md5(param.verify),
                    expireDate: expireDate.format('YYYY-MM-DD HH:mm:ss'),
                },
            );

            return { error: false, notice: 'success' };
        }
        throw (ErrorCodes.error('account_not_found'));
    }
};
