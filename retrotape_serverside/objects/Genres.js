const md5 = require('md5');
const moment = require('moment');
const { CfwObject } = require('corefwnode');

module.exports = class Genres extends CfwObject
{
    constructor(session)
    {
        const proxy = super(session);

        return proxy;
    }

    tableConf()
    {
        return {
            tableName: 'genre',
            id: {
                idgenre: {
                    fieldType: 'int',
                    req: 0,
                },
            },
            ownerField: 'idgenre',
            fields: {
                genreName: {
                    fieldType: 'text',
                    maxlength: 45,
                    minlength: 2,
                    req: 1,
                },
            },
            onduplicate: ['genreName'],
        };
    };

    async insertRow(param)
    {
        const me = this;
        try
        {
            me.db = await me.db.begin(me.session);
            me.genreName = param.genreName;
            const val = await me.insert();

            param.id = { idGenre: val.lastId };
            const genres = new Genre(me.session);
            genres.db = me.db;
            await genres.insertRow(param);
            await me.db.commit();
            return { error: false, notice: 'success' };
        }
        catch (e)
        {
            me.db.rollback();
            throw (e);
        }
    }

    async updateRow(param)
    {
        const me = this;
        try
        {
            me.db = await me.db.begin(me.session);
            if (param.password && param.password !== '')
            {
                param.password = await me.session.getHashedPassword(param.password);
            }
            else
            {
                delete (param.password);
            }

            for (const key in param)
            {
                if (param[key] !== undefined)
                {
                    me[key] = param[key];
                }
            }
            await me.update();
            const genres = new Genre(me.session);
            genres.id = param.id;
            try
            {
                await genres.select();
                await genres.updateRow(param);
            }
            catch (error)
            {
                const secParams = { ...param };
                secParams.id = { idgenre: param.id };
                await genres.insertRow(secParams);
            }

            await me.db.commit();
            return { error: false, notice: 'success' };
        }
        catch (e)
        {
            me.db.rollback();
            throw (e);
        }
    }

}