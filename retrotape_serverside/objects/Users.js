const md5 = require('md5');
const moment = require('moment');
const { CfwObject } = require('corefwnode');
const UsersData = require('./UsersData.js');

module.exports = class Users extends CfwObject
{
    constructor(session)
    {
        const proxy = super(session);

        return proxy;
    }

    tableConf()
    {
        return {
            tableName: 'users',
            id: {
                userId: {
                    fieldType: 'int',
                    maxlength: 20,
                    req: 0,
                },
            },
            ownerField: 'userId',
            companyField: 'companyId',
            fields: {
                companyId: {
                    fieldType: 'int',
                    maxlength: 20,
                    req: 1,
                },
                username: {
                    fieldType: 'text',
                    maxlength: 255,
                    minlength: 1,
                    req: 1,
                },
                password: {
                    fieldType: 'text',
                    maxlength: 255,
                    minlength: 5,
                    req: 1,
                },
                userEmail: {
                    fieldType: 'email',
                    maxlength: 255,
                    minlength: 5,
                    req: 1,
                },
                groupId: {
                    fieldType: 'int',
                    maxlength: 20,
                    minlength: 1,
                    req: 0,
                },
                dateEntered: {
                    fieldType: 'dateTime',
                    req: 1,
                },
                status: {
                    fieldType: 'text',
                    maxlength: 255,
                    req: 1,
                },
            },
        };
    }


    async selectRow(param)
    {
        const me = this;

        let data = [];
        let dir = 'DESC';

        let sort = `u.${Object.keys(me.idField)[0]}`;

        if (typeof param.sort !== 'undefined')
        {
            sort = `u.${me.db.escape(param.sort)}`;
        }

        if (typeof param.dir !== 'undefined')
        {
            dir = me.db.escape(param.dir);
        }

        let limit = '';
        if (typeof param.start !== 'undefined' && typeof param.limit !== 'undefined')
        {
            const start = me.db.escape(parseInt(param.start, 10));
            const end = me.db.escape(parseInt(param.limit, 10));
            limit = ` LIMIT ${start}, ${end} `;
        }

        let whereSearch = '';
        const params = {};
        if (typeof param.query !== 'undefined' && param.query !== '')
        {
            const searchString = [];
            for (const key in me.tableConfig.fields)
            {
                if (me.tableConfig.fields[key])
                {
                    const value = me.tableConfig.fields[key];
                    if (value.fieldType === 'date' || value.fieldType === 'dateTime')
                    {
                        continue;
                    }
                    searchString.push(` u.${key} LIKE :${key} `);
                    params[key] = `%${param.query}%`;
                }
            }
            if (searchString.length > 0)
            {
                whereSearch = ` AND (${searchString.join(' OR ')}) `;
            }
        }

        if (typeof param.conditions !== 'undefined' && param.conditions !== '')
        {
            for (const key in param.conditions)
            {
                if (param.conditions[key])
                {
                    const value = me.db.escape(param.conditions[key]);
                    whereSearch += ` AND u.${value} = :${value} `;
                    params[value] = param.bind[key];
                }
            }
        }

        if (typeof param.filter !== 'undefined' && param.filter !== '')
        {
            const filters = JSON.parse(param.filter);
            for (const key in filters)
            {
                if (filters[key])
                {
                    const value = filters[key];
                    const keySec = me.db.escape(value.property);
                    whereSearch += ` AND u.${keySec} LIKE :${keySec} `;
                    params[keySec] = `%${value.value}%`;
                }
            }
        }

        let ownerWhere = '';
        if (me.ownerActionGlob === true)
        {
            ownerWhere = ` AND u.${me.tableConfig.ownerField}=:sessionUserId `;
            params.sessionUserId = me.session.userId;
        }

        if (me.companyActionGlob === true)
        {
            ownerWhere += ` AND u.${me.tableConfig.companyField}=:sessionCompanyId `;
            params.sessionCompanyId = me.session.companyId;
        }

        let results = await me[me.dbName].query(`SELECT 
            ud.*,u.*, uc.companyName
            FROM \`${me.tableName}\` u
            LEFT JOIN users_data ud ON ud.userId=u.userId 
            LEFT JOIN user_companies uc ON uc.companyId=u.companyId 
            WHERE 1=1 ${ownerWhere} ${whereSearch} 
            ORDER BY ${sort} ${dir} ${limit}`, params);

        for (const result of results)
        {
            data.push(result);
        }

        if (typeof param.initialId !== 'undefined')
        {
            data = await me.findInitialId(data, param.initialId);
        }

        results = await me[me.dbName].query(`SELECT COUNT(*) AS cnt 
        FROM \`${me.tableName}\` u 
        WHERE 1=1 ${ownerWhere} ${whereSearch}`, params);

        const numRows = results[0].cnt;

        return { root: data, totalCount: numRows };
    }

    async insertRow(param)
    {
        const me = this;
        try
        {
            me.db = await me.db.begin(me.session);
            me.username = param.username;
            me.password = await me.session.getHashedPassword(param.password);
            me.userEmail = param.userEmail;
            me.dateEntered = moment.utc().format('YYYY-MM-DD HH:mm:ss');
            me.groupId = 2;
            me.status = 'active';
            me.userId = false;
            const val = await me.insert();

            param.id = { iduser: val.lastId };
            const userData = new UsersData(me.session);
            userData.db = me.db;
            await userData.insertRow(param);
            await me.db.commit();
            return { error: false, notice: 'success' };
        }
        catch (e)
        {
            me.db.rollback();
            throw (e);
        }
    }

}