const md5 = require('md5');
// const db = require('../../retrotape');
const moment = require('moment');
const { CfwObject } = require('corefwnode');

module.exports = class Films extends CfwObject
{
    constructor(session)
    {
        const proxy = super(session);
        return proxy;
    }

    tableConf()
    {
        return {
            tableName: 'film',
            id: {
                idfilm: {
                    fieldType: 'int',
                    req: 0,
                },
            },
            ownerField: 'idfilm',
            fields: {
                filmName: {
                    fieldType: 'text',
                    maxlength: 45,
                    minlength: 2,
                    req: 1,
                },
                starring: {
                    fieldType: 'text',
                    maxlength: 255,
                    minlength: 2,
                    req: 1,
                },
                genreId: {
                    fieldType: 'int',
                    maxlength: 255,
                    req: 1,
                    null: false,
                },
                description: {
                    fieldType: 'text',
                    maxlength: 255,
                    minlength: 2,
                    req: 1,
                },
            },
            onduplicate: ['filmName','starring', 'genreId', 'description'],
        };
    }

    async insertRow(param)
    {
        const me = this;
        try
        {
            me.db = await me.db.begin(me.session);
            me.filmName = param.filmName;
            me.starring = param.starring;
            me.genreId = param.genreId;
            me.description = param.description;
            const val = await super.insert();

            // param.id = { idfilm: val.lastId };
            // const films = new Films(me.session);
            // films.db = me.db;
            await me.db.commit();
            return { error: false, notice: 'success' };
        }
        catch (e)
        {
            me.db.rollback();
            throw (e);
        }
    }
    async updateRow(param)
    {
        const me = this;
        try
        {
            me.db = await me.db.begin(me.session);
            if (param.password && param.password !== '')
            {
                param.password = await me.session.getHashedPassword(param.password);
            }
            else
            {
                delete (param.password);
            }

            for (const key in param)
            {
                if (param[key] !== undefined)
                {
                    me[key] = param[key];
                }
            }
            await super.update();
            // const films = new Films(me.session);
            // films.id = param.id;
            /* try
            {
                await films.select();
                await films.updateRow(param);
            }
            catch (error)
            {
                const secParams = { ...param };
                secParams.id = { idfilm: param.id };
                await films.insertRow(secParams);
            } */

            await me.db.commit();
            return { error: false, notice: 'success' };
        }
        catch (e)
        {
            me.db.rollback();
            throw (e);
        }
    }

    async selectRow(param)
    {
        const me = this;

        let data = [];
        let dir = 'DESC';

        let sort = `u.${Object.keys(me.idField)[0]}`;

        if (typeof param.sort !== 'undefined')
        {
            sort = `u.${me.db.escape(param.sort)}`;
        }

        if (typeof param.dir !== 'undefined')
        {
            dir = me.db.escape(param.dir);
        }

        let limit = '';
        if (typeof param.start !== 'undefined' && typeof param.limit !== 'undefined')
        {
            const start = me.db.escape(parseInt(param.start, 10));
            const end = me.db.escape(parseInt(param.limit, 10));
            limit = ` LIMIT ${start}, ${end} `;
        }

        // let whereSearch = '';
        // const params = {};
        // if (typeof param.query !== 'undefined' && param.query !== '')
        // {
        //     const searchString = [];
        //     for (const key in me.tableConfig.fields)
        //     {
        //         if (me.tableConfig.fields[key])
        //         {
        //             const value = me.tableConfig.fields[key];
        //             if (value.fieldType === 'date' || value.fieldType === 'dateTime')
        //             {
        //                 continue;
        //             }
        //             searchString.push(` u.${key} LIKE :${key} `);
        //             params[key] = `%${param.query}%`;
        //         }
        //     }
        //     if (searchString.length > 0)
        //     {
        //         whereSearch = ` AND (${searchString.join(' OR ')}) `;
        //     }
        // }

        // if (typeof param.conditions !== 'undefined' && param.conditions !== '')
        // {
        //     for (const key in param.conditions)
        //     {
        //         if (param.conditions[key])
        //         {
        //             const value = me.db.escape(param.conditions[key]);
        //             whereSearch += ` AND u.${value} = :${value} `;
        //             params[value] = param.bind[key];
        //         }
        //     }
        // }

        // if (typeof param.filter !== 'undefined' && param.filter !== '')
        // {
        //     const filters = JSON.parse(param.filter);
        //     for (const key in filters)
        //     {
        //         if (filters[key])
        //         {
        //             const value = filters[key];
        //             const keySec = me.db.escape(value.property);
        //             whereSearch += ` AND u.${keySec} LIKE :${keySec} `;
        //             params[keySec] = `%${value.value}%`;
        //         }
        //     }
        // }

        // let ownerWhere = '';
        // if (me.ownerActionGlob === true)
        // {
        //     ownerWhere = ` AND u.${me.tableConfig.ownerField}=:sessionUserId `;
        //     params.sessionUserId = me.session.userId;
        // }

        // if (me.companyActionGlob === true)
        // {
        //     ownerWhere += ` AND u.${me.tableConfig.companyField}=:sessionCompanyId `;
        //     params.sessionCompanyId = me.session.companyId;
        // }

        let results = await me[me.dbName].query(`SELECT 
            film.*, genre.genreName
            FROM \`${me.tableName}\` film 
            INNER JOIN genre ON film.genreId=genre.idgenre`);
        for (const result of results)
        {
            data.push(result);
        }

        if (typeof param.initialId !== 'undefined')
        {
            data = await me.findInitialId(data, param.initialId);
        }

        results = await me[me.dbName].query(`SELECT COUNT(*) AS cnt 
        FROM \`${me.tableName}\` u`);

        const numRows = results[0].cnt;

        return { root: data, totalCount: numRows };
    }

}