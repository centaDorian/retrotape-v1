const fs = require('fs');

/**
 * Generator object
 *
 * @version $Id$
 * @copyright 2018
 */

module.exports = class Generator
{
    constructor(session)
    {
        const me = this;
        me.lib = global.lib;
        me.config = lib.config;
        me.session = session;
        me.db = me.lib.db;
    }

    async saveModule(params)
    {
        const me = this;

        const { formData } = params;
        const { fields } = params;

        const ids = {};
        const fieldsData = {};
        const sqlPrimaryFields = [];
        const sqlFields = [];
        const tableInputs = [];
        const tableHeaders = [];
        const moduleInputs = [];
        const formDataVals = {};
        let primaryKey = 'id';

        for (let i = 0; i < fields.length; i += 1)
        {
            const field = fields[i];

            if (typeof field.fieldType === 'undefined')
            {
                throw ({ error: true, notice: me.session.translate('FIELDTYPEMUSTBEDEFINED') });
            }

            const input = `<div class="row">
          <div class="col-sm-12">
            <b-form-fieldset v-bind:label="$t('${field.name.toUpperCase()}')">
              <inputvalidation 
              :formerror="formError" 
              name="${field.name}">
                <b-form-input 
                type="text" 
                v-model="formData.${field.name}" 
                v-bind:placeholder="$t('${field.name.toUpperCase()}')"></b-form-input>
              </inputvalidation>
            </b-form-fieldset>
          </div>
        </div>`;

            if (typeof field.primary !== 'undefined' && field.primary === 1)
            {
                ids[field.name] = {
                    fieldType: field.fieldType,
                    req: (typeof field.required !== 'undefined' && field.required === 1) ? 1 : 0,
                };

                if (typeof field.maxLength !== 'undefined' && field.maxLength !== '')
                {
                    ids[field.name].maxlength = field.maxLength;
                }
                if (typeof field.minLength !== 'undefined' && field.minLength !== '')
                {
                    ids[field.name].minlength = field.minLength;
                }
                if (typeof field.maxValue !== 'undefined' && field.maxValue !== '')
                {
                    ids[field.name].maxvalue = field.maxValue;
                }
                if (typeof field.minValue !== 'undefined' && field.minValue !== '')
                {
                    ids[field.name].minvalue = field.minValue;
                }

                if (typeof field.decorators !== 'undefined')
                {
                    ids[field.name].decorators = field.decorators;
                }

                sqlPrimaryFields.push(`\`${field.name}\``);

                if (typeof field.required !== 'undefined' && field.required === 1)
                {
                    tableHeaders.push(`<th>{{$t('${field.name.toUpperCase()}')}}</th>`);
                    tableInputs.push(`<td><b-form-input type="text" v-model="item.${field.name}"></b-form-input></td>`);
                    moduleInputs.push(input);
                }
                else
                {
                    tableHeaders.push('<th>{{$t(\'ID\')}}</th>');
                    tableInputs.push(`<td>{{item.${field.name}}}</td>`);
                    formDataVals.id = '';
                    primaryKey = field.name;
                }
            }
            else
            {
                fieldsData[field.name] = {
                    fieldType: field.fieldType,
                    req: (typeof field.required !== 'undefined' && field.required === 1) ? 1 : 0,
                };

                if (typeof field.maxLength !== 'undefined' && field.maxLength !== '')
                {
                    fieldsData[field.name].maxlength = parseInt(field.maxLength, 10);
                }
                if (typeof field.minLength !== 'undefined' && field.minLength !== '')
                {
                    fieldsData[field.name].minlength = parseInt(field.minLength, 10);
                }
                if (typeof field.maxValue !== 'undefined' && field.maxValue !== '')
                {
                    fieldsData[field.name].maxvalue = parseInt(field.maxValue, 10);
                }
                if (typeof field.minValue !== 'undefined' && field.minValue !== '')
                {
                    fieldsData[field.name].minvalue = parseInt(field.minValue, 10);
                }

                if (typeof field.decorators !== 'undefined')
                {
                    fieldsData[field.name].decorators = field.decorators;
                }

                tableHeaders.push(`<th>{{$t('${field.name.toUpperCase()}')}}</th>`);
                tableInputs.push(`<td><b-form-input type="text" v-model="item.${field.name}"></b-form-input></td>`);
                moduleInputs.push(input);
                formDataVals[field.name] = '';
            }

            let type = 'varchar';
            let sign = '';
            switch (field.fieldType)
            {
            case 'int':
                type = 'int';
                sign = 'unsigned';
                break;
            case 'float':
                type = 'float';
                sign = 'unsigned';
                break;
            case 'text':
            case 'string':
            case 'email':
                type = 'varchar';
                break;
            case 'dateTime':
                type = 'datetime';
                break;
            case 'date':
                type = 'date';
                break;
            case 'time':
                type = 'time';
                break;
            default:
                type = 'varchar';
                break;
            }

            if (type === 'varchar' && field.maxLength > 255)
            {
                type = 'text';
            }
            if ((type === 'varchar' || type === 'int' || type === 'float') && field.maxLength === '')
            {
                throw ({ error: true, notice: me.session.translate('VARCHARANDINTMUSTHAVELENGTHDEFINED') });
            }

            let length = '';
            if (typeof field.maxLength !== 'undefined'
            && field.maxLength != null
            && field.maxLength <= 255
            && field.maxLength !== '')
            {
                length = `(${field.maxLength})`;
            }

            let nullVal = 'NOT NULL';
            if (typeof field.null !== 'undefined' && field.null === 1)
            {
                nullVal = 'NULL';
            }

            let autoincr = '';
            if (typeof field.primary !== 'undefined'
            && field.primary === 1
            && (typeof field.required === 'undefined' || field.required === 0))
            {
                autoincr = 'AUTO_INCREMENT';
            }

            if (type === 'int' || type === 'float' || type === 'datetime')
            {
                sqlFields.push(` \`${field.name}\` ${type}${length} ${sign} ${nullVal} ${autoincr} `);
            }
            else
            {
                sqlFields.push(` \`${field.name}\` ${type}${length} COLLATE utf8_unicode_ci ${nullVal} ${autoincr} `);
            }
        }

        if (Object.keys(ids).length === 0)
        {
            throw ({ error: true, notice: me.session.translate('IDFIELDMUSTBEDEFINED') });
        }

        await me.db.query(`CREATE TABLE IF NOT EXISTS \`${formData.tableName}\` (${
            sqlFields.join(', ')
        },`
      + `PRIMARY KEY (${sqlPrimaryFields.join(',')})`
      + ') ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci');


        const objectStr = `module.exports = class ${formData.moduleName} extends CfwObject
            {
              tableConf()
              {
                return {
                  'tableName': '${formData.tableName}',
                  'id': ${JSON.stringify(ids)},
                  'fields': ${JSON.stringify(fieldsData)}
                };
              }

            }
      `;

        fs.writeFileSync(`${__dirname}/${formData.moduleName}.js`, objectStr);

        let viewStr = me.returnTemplate();
        viewStr = viewStr.replace('REPEATMODULEINPUTS', moduleInputs.join(''));
        viewStr = viewStr.replace(/MODULENAMEUPPERCASE/g, formData.moduleName.toUpperCase());
        viewStr = viewStr.replace(/MODULENAME/g, formData.moduleName);
        viewStr = viewStr.replace('REPEATTABLEHEADERS', tableHeaders.join(''));
        viewStr = viewStr.replace('REPEATTABLEINPUTS', tableInputs.join(''));
        viewStr = viewStr.replace('FORMDATAVALS', JSON.stringify(formDataVals));
        viewStr = viewStr.replace(/PRIMARYKEY/g, primaryKey);

        fs.writeFileSync(`../${me.config.clientSideLocation}/src/views/admin/${formData.moduleName}.vue`, viewStr);

        let apiStr = me.apiTemplate();
        apiStr = apiStr.replace(/MODULENAME/g, formData.moduleName);

        fs.writeFileSync(`../${me.config.clientSideLocation}/src/api/admin/${formData.moduleName}.js`, apiStr);


        let data = fs.readFileSync(`../${me.config.clientSideLocation}/src/containers/Full.vue`, 'utf8');

        let str = `,
      {
        name: me.$i18n.t('${formData.moduleName.toUpperCase()}'),
        url: '/admin/system/${formData.moduleName.toLowerCase()}',
        icon: 'rocket',
        visible: (me.globals.acl.${formData.moduleName}
            && me.globals.acl.${formData.moduleName}.instantiate)
            ? true
            : false,
      }//GENERATORINSERT`;

        data = data.replace('//GENERATORINSERT', str);

        fs.writeFileSync(`../${me.config.clientSideLocation}/src/containers/Full.vue`, data);


        data = fs.readFileSync(`../${me.config.clientSideLocation}/src/router/index.js`, 'utf8');

        str = `,
      {
        path: '${formData.moduleName.toLowerCase()}',
        name: '${formData.moduleName.toUpperCase()}',
        component: ${formData.moduleName}
      }//GENERATORINSERT`;

        data = data.replace('//GENERATORINSERT', str);


        str = `import ${formData.moduleName} from '@/views/admin/${formData.moduleName}'
      //GENERATORLASTITEM`;

        data = data.replace('//GENERATORLASTITEM', str);

        fs.writeFileSync(`../${me.config.clientSideLocation}/src/router/index.js`, data);

        return { error: false, notice: me.session.translate('SUCCESS') };
    }

    static returnTemplate()
    {
        return `<template>
      <div class="animated fadeIn">
        <div class="row">
          <div class="col-sm-12">
            <b-card>
              <div slot="header">
                <strong>{{$t('NEWMODULENAMEUPPERCASE')}}</strong>
              </div>
              REPEATMODULEINPUTS
              <div slot="footer">
                <p class="bg-danger" 
                v-if="formError.error && typeof formError.notice == 'string'">{{formError.notice}}</p>
                <b-button 
                type="submit" 
                size="sm" 
                variant="primary" 
                v-on:click.prevent="saveRow(formData)"><i class="fa fa-dot-circle-o"></i> {{$t('SUBMIT')}}</b-button>
              </div>
            </b-card>
          </div><!--/.col-->
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                  <i class="fa fa-align-justify"></i> {{$t('MODULENAMEUPPERCASE')}}
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <b-form-fieldset v-bind:label="$t('SEARCH')">
                        <b-form-input v-model="pagination.query"></b-form-input>
                      </b-form-fieldset>
                    </div>
                  </div>
                </div>
                <div class="card-body" v-if="typeof data.root == 'undefined' || !data.root.length">
                  <p class="center">{{$t('NODATAFOUND')}}</p>
                </div>
                <div class="card-body table-responsive" v-if="typeof data.root != 'undefined' && data.root.length">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        REPEATTABLEHEADERS
                        <th>{{$t('SAVE')}}</th>
                        <th>{{$t('DELETE')}}</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="item in data.root">
                        REPEATTABLEINPUTS
                        <td><b-button 
                        type="reset" 
                        size="sm" 
                        variant="primary" 
                        v-on:click.prevent="saveRow(item)"><icon icon="save"></icon> {{$t('SAVE')}}</b-button></td>
                        <td><b-button 
                        type="reset" 
                        size="sm" 
                        variant="danger" 
                        v-on:click.prevent="deleteRow(item.PRIMARYKEY)">
                        <icon icon="ban"></icon> {{$t('DELETE')}}</b-button></td>
                      </tr>
                    </tbody>
                  </table>
                  <nav>
                    <b-pagination 
                    align="center" 
                    :total-rows="parseInt(data.totalCount)" 
                    v-model="pagination.currentPage" 
                    :per-page="pagination.limit"></b-pagination>
                  </nav>
                </div>
              </div>
            </div>
        </div>
      </div>
    </template>

    <script>

    import { mapActions, mapGetters } from 'vuex'
    import Api from '@/api/admin/MODULENAME'

    export default {
      name: 'MODULENAME',
      data: function()
      {
        return {
          data: {},
          formError: {},
          formData: FORMDATAVALS,
          pagination:
          {
            query: '',
            currentPage: 1,
            limit: 20,
            searchTimeout: false
          }
        }
      },
      created: function()
      {
        var me = this;
        me.getData();
      },
      methods:
      {
        async saveRow(item)
        {
          var me = this;
          try
          {
            if(typeof item.PRIMARYKEY != 'undefined')
            {
              item.id = item.PRIMARYKEY;
              delete(item.PRIMARYKEY);
            }

            var val = await Api.save(item);

            if(val.error)
            {
              me.$notify.notice(val);
              me.formError = val;
            }
            else
            {
              me.$notify.notice(val);
              me.formError = {};
              me.getData();
            }
          }
          catch(error)
          {
            me.$notify.notice(error);
          }
        },
        async getData()
        {
          var me = this;
          try
          {
            var data = await Api.selectRow(me.getParams);

            if(data.error && data.error == true)
            {
              me.$notify.notice(data);
              return;
            }
            me.data = data;
          }
          catch(error)
          {
            me.$notify.notice(error);
          }
        },
        async deleteRow(id)
        {
          var me = this;
          me.$store.commit('setModalAlert',
          {
            text: me.$i18n.t('AREYOUSHURE'),
            btnAction: async function()
            {
              try
              {
                var data = await Api.deleteRow({id: id});
                me.$notify.notice(data);
                me.getData();
              }
              catch(error)
              {
                me.$notify.notice(error);
              }
            }
          })
        }
      },
      watch:
      {
        'pagination.currentPage': function(val)
        {
          var me = this;
          me.getData();
        },
        'pagination.query': function(val)
        {
          var me = this;

          if(me.pagination.searchTimeout != false) clearTimeout(me.pagination.searchTimeout);

          me.pagination.searchTimeout = setTimeout(function()
          {
            me.getData();
            me.pagination.searchTimeout = false;
          }, 300);
        }
      },
      computed:
      {
        getParams: function()
        {
          var me = this;
          var data = Object.assign({}, me.pagination, {});
          data.start = (data.currentPage-1)*data.limit;
          return data;
        }
      }
    }
    </script>
`;
    }

    static apiTemplate()
    {
        return `import axios from '@/lib/AxApi';

    export default
    {
        async selectRow(params)
        {
            try
            {
                var response = await axios.get('/MODULENAME/selectRow',
                {
                    params: params
                });
    
                return response.data;
            }
            catch (error)
            {
                throw (error.toString())
            }
        },
        async save(params)
        {
            try
            {
              var response;
                if (typeof params.id != 'undefined' && params.id != '')
                {
                    response = await axios.post('/MODULENAME/updateRow', params)
                }
                else
                {
                    response = await axios.post('/MODULENAME/insertRow', params)
                }
                return response.data;
            }
            catch (error)
            {
                throw (error.toString())
            }
        },
        async deleteRow(params)
        {
            try
            {
              var response = await axios.post('/MODULENAME/deleteRow', params);
              return response.data;
            }
            catch (error)
            {
                throw (error.toString())
            }
        }
    }`;
    }
};
