const { ObjClasses } = require('corefwnode');
/**
 * Translator object
 *
 * @version $Id$
 * @copyright 2008
 */
module.exports = class Translator extends ObjClasses.translator.class
{
    getTranslationByLangId(param)
    {
        const me = this;
        return new Promise((resolve) =>
        {
            this.db.query('UPDATE session SET langId=? WHERE sessionId=?', [param.langId, me.session.sessionId], () =>
            {
                resolve({ root: me.lang.trans[parseInt(param.langId, 10)], key: param.langId });
            });
        });
    }
};
