module.exports = {
  devServer: {
    proxy: 'http://localhost:8000'
  },
  configureWebpack: {
    devtool: 'source-map'
  },
	baseUrl: '/admin/',
  outputDir: '../retrotape_serverside/admin/'
}
