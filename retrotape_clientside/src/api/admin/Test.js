import axios from '@/lib/AxApi';

    export default {
      selectRow(params, cb)
      {
        axios.get('/Test/selectRow',
        {
          params: params
        })
        .then(response => {
          cb(response.data);
        })
        .catch(e => {
          //TODO
        })
      },
      save(params, cb)
      {
        if(typeof params.id != 'undefined' && params.id != '')
        {
          axios.post('/Test/updateRow', params)
          .then(response => {
            cb(response.data);
          })
          .catch(e => {
            //TODO
          })
        }
        else
        {
          axios.post('/Test/insertRow', params)
          .then(response => {
            cb(response.data);
          })
          .catch(e => {
            //TODO
          })
        }
      },
      deleteRow(params, cb)
      {
        axios.post('/Test/deleteRow', params)
        .then(response => {
          cb(response.data);
        })
        .catch(e => {
          //TODO
        })
      }
    }
