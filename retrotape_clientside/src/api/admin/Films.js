import axios from '@/lib/AxApi';

export default {
  selectRow(params, cb)
  {
    axios.get('/Films/selectRow',
    {
      params: params
    })
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },

  save(params, cb)
  {
    if(typeof params.id != 'undefined' && params.id != '')
    {
      axios.post('/Films/updateRow', params)
      .then(response => {
        cb(response.data);
      })
      .catch(e => {
        //TODO
      })
    }
    else
    {
      axios.post('/Films/insertRow', params)
      .then(response => {
        cb(response.data);
      })
      .catch(e => {
        //TODO
      })
    }
  },
  deleteRow(params, cb)
  {
    axios.post('/Films/deleteRow', params)
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  }
}