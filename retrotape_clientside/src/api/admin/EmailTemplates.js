import axios from '@/lib/AxApi';

export default {
  selectRow(params, cb)
  {
    axios.get('/EmailTemplates/selectRow',
    {
      params: params
    })
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  save(params, cb)
  {
    if(typeof params.id != 'undefined' && params.id != '')
    {
      axios.post('/EmailTemplates/updateRow', params)
      .then(response => {
        cb(response.data);
      })
      .catch(e => {
        //TODO
      })
    }
    else
    {
      axios.post('/EmailTemplates/insertRow', params)
      .then(response => {
        cb(response.data);
      })
      .catch(e => {
        //TODO
      })
    }
  },
  deleteRow(params, cb)
  {
    axios.post('/EmailTemplates/deleteRow', params)
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  }
}
