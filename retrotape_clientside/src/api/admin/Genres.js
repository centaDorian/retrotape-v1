import axios from '@/lib/AxApi';

export default {
  selectRow(params, cb)
  {
    axios.get('/Genres/selectRow',
    {
      params: params
    })
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },

  save(params, cb)
  {
    if(typeof params.id != 'undefined' && params.id != '')
    {
      axios.post('/Genres/updateRow', params)
      .then(response => {
        cb(response.data);
      })
      .catch(e => {
        //TODO
      })
    }
    else
    {
      axios.post('/Genres/insertRow', params)
      .then(response => {
        cb(response.data);
      })
      .catch(e => {
        //TODO
      })
    }
  },

  deleteRow(params, cb)
  {
    axios.post('/Genres/deleteRow', params)
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  }

}