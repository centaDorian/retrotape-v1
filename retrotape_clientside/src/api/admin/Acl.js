import axios from '@/lib/AxApi';

export default {
  getGroups(params, cb)
  {
    axios.get('/AclPrivilages/selectRow',
    {
      params: params
    })
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  getPrivilagesByGroupId(params, cb)
  {
    axios.get('/AclPrivilages/getPrivilagesByGroupId',
    {
      params: params
    })
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  updatePrivilageByGroupId(params, cb)
  {
    axios.post('/AclPrivilages/updatePrivilageByGroupId', params)
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  saveAcl(params, cb)
  {
    axios.post('/AclObjects/insertRow', params)
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  deleteAcl(params, cb)
  {
    axios.post('/AclObjects/deleteRow', params)
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  }
}
