import axios from '@/lib/AxApi';

export default {
  getLanguageTranslations(params, cb)
  {
    axios.get('/Translator/getLanguageTranslations',
    {
      params: params
    })
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  saveTrans(params, cb)
  {
    axios.post('/Translator/saveTrans', params)
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  deleteTrans(params, cb)
  {
    axios.post('/Translator/deleteTrans', params)
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  emptyUntranslated(cb)
  {
    axios.post('/Translator/emptyUntranslated')
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  }
}
