import axios from '@/lib/AxApi';

export default {
  selectRow(params, cb)
  {
    axios.get('/Fees/selectRow',
    {
      params: params
    })
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },

  save(params, cb)
  {
    if(typeof params.id != 'undefined' && params.id != '')
    {
      axios.post('/Fees/updateRow', params)
      .then(response => {
        cb(response.data);
      })
      .catch(e => {
        //TODO
      })
    }
    else
    {
      axios.post('/Fees/insertRow', params)
      .then(response => {
        cb(response.data);
      })
      .catch(e => {
        //TODO
      })
    }
  },
  deleteRow(params, cb)
  {
    axios.post('/Fees/deleteRow', params)
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  }
}