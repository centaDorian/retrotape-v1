import axios from '@/lib/AxApi';

export default
{
    getWikiByName(params, cb)
    {
        axios.get('/Wiki/getWikiByName',
            {
                params: params
            })
            .then(response =>
            {
                cb(response.data);
            })
            .catch(e =>
            {
                //TODO
            })
    },
    getWikiModules(params, cb)
    {
        axios.get('/Wiki/getWikiModules',
            {
                params: params
            })
            .then(response =>
            {
                cb(response.data);
            })
            .catch(e =>
            {
                //TODO
            })
    },
    getSingleWiki(params, cb)
    {
        axios.get('/Wiki/getSingleWiki',
            {
                params: params
            })
            .then(response =>
            {
                cb(response.data);
            })
            .catch(e =>
            {
                //TODO
            })
    },
    selectRow(params, cb)
    {
        axios.get('/Wiki/selectRow',
            {
                params: params
            })
            .then(response =>
            {
                cb(response.data);
            })
            .catch(e =>
            {
                //TODO
            })
    },
    save(params, cb)
    {
        if (typeof params.id != 'undefined' && params.id != '')
        {
            axios.post('/Wiki/updateRow', params)
                .then(response =>
                {
                    cb(response.data);
                })
                .catch(e =>
                {
                    //TODO
                })
        }
        else
        {
            axios.post('/Wiki/insertRow', params)
                .then(response =>
                {
                    cb(response.data);
                })
                .catch(e =>
                {
                    //TODO
                })
        }
    },
    deleteRow(params, cb)
    {
        axios.post('/Wiki/deleteRow', params)
            .then(response =>
            {
                cb(response.data);
            })
            .catch(e =>
            {
                //TODO
            })
    }
}