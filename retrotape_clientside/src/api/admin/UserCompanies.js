import axios from '@/lib/AxApi';

export default {
  selectRow(params, cb)
  {
    axios.get('/UserCompanies/selectRow',
    {
      params: params
    })
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  save(params, cb)
  {
    if(typeof params.id != 'undefined' && params.id != '')
    {
      axios.post('/UserCompanies/updateRow', params)
      .then(response => {
        cb(response.data);
      })
      .catch(e => {
        //TODO
      })
    }
    else
    {
      axios.post('/UserCompanies/insertRow', params)
      .then(response => {
        cb(response.data);
      })
      .catch(e => {
        //TODO
      })
    }
  },
  deleteRow(params, cb)
  {
    axios.post('/UserCompanies/deleteRow', params)
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  }
}
