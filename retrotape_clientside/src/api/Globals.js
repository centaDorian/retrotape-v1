import axios from '@/lib/AxApi';

export default {
  getGlobals(cb)
  {
    axios.get('/Session/returnJsVars')
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  login(data, cb)
  {
    axios.post('/Session/loginUser', data)
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  logout(cb)
  {
    axios.post('/Session/logOut', {})
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  fetchLanguage(langId, cb)
  {
    axios.get('/Translator/getTranslationByLangId',
    {
      params: {
        langId: langId
      }
    })
    .then(response => {
      cb(response.data);
    })
    .catch(e => {
      //TODO
    })
  },
  checkTrans: function(key)
  {
    axios.post('/Translator/checkTrans', {key: key})
  }
}
