import axios from 'axios';


const client = () =>
{
    var defaultOptions = {};
    return {
        get: (url, options = {}) => axios.get(url, { ...defaultOptions, ...options }),
        post: function(url, params, options = {})
        {
          var formData = false;
          for(var key in params)
          {
            if(typeof params[key] == 'Object') formData = true;
          }

          if(formData)
          {
            defaultOptions = {
                headers: {
                    'content-type': 'multipart/form-data',
                }
            };

            let data = new FormData();
            for(var key in params)
            {
              if(typeof params[key] == 'Object') data.append(key, params[key], params[key].name);
              else data.append(key, params[key]);
            }
            return axios.post(url, data, { ...defaultOptions, ...options });
          }
          else return axios.post(url, params, { ...defaultOptions, ...options });
        },
        put: (url, data, options = {}) => axios.put(url, data, { ...defaultOptions, ...options }),
        delete: (url, options = {}) => axios.delete(url, { ...defaultOptions, ...options }),
    };
};

export default client()
