import globals from '../../api/Globals'

// initial state
const state = {
  globals: {auth: 0},
  menuActive: false,
  messages: [],
  loginResponse: false,
  translation: {},
  langInitialized: false,
  modalAlert: false
}

// getters
const getters = {
  globals: state => state.globals,
  menuActive: state => state.menuActive,
  messages: state => state.messages,
  loginResponse: state => state.loginResponse,
  translation: state => state.translation,
  langInitialized: state => state.langInitialized,
  modalAlert: state => state.modalAlert
}

// actions
const actions = {
  getGlobals ({ commit }) {
    globals.getGlobals(globals => {
      commit('RECEIVE_GLOBALS', { globals })
    })
  },
  fetchLanguage: function (context, data) {
    globals.fetchLanguage(data, translation => {
      context.commit('RECEIVE_LANGUAGE', { translation })
    })
  },
  login: function (context, data) {
    globals.login(data, loginResponse => {
      context.commit('LOGIN_RESPONSE', { loginResponse })
      globals.getGlobals(globals => {
        context.commit('RECEIVE_GLOBALS', { globals })
      })
    })
  },
  logout ({ commit }) {
    globals.logout(function(context)
    {
      globals.getGlobals(globals => {
        commit('RECEIVE_GLOBALS', { globals })
      })
    })
  }
}

// mutations
const mutations = {
  ['RECEIVE_GLOBALS'] (state, { globals }) {
    state.globals = globals
  },
  ['RECEIVE_LANGUAGE'] (state, { translation }) {
    state.langInitialized = true;
    state.translation = translation
  },
  ['LOGIN_RESPONSE'] (state, { loginResponse }) {
    state.loginResponse = loginResponse
  },
  setModalAlert: function(state, modalAlert) {
    state.modalAlert = modalAlert
  },
  toggleMenu: state => state.menuActive = !state.menuActive
}

export default {
  state,
  getters,
  actions,
  mutations
}
