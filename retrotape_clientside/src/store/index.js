import Vue from 'vue'
import Vuex from 'vuex'
import globals from './modules/globals'

Vue.use(Vuex)

// the root, initial state object
const store = new Vuex.Store({
  modules: {
      globals
  }
});

// create the Vuex instance by combining the state and mutations objects
// then export the Vuex store for use by our components
export default store
