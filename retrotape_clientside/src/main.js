// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import store from './store/index'
import VueI18n from 'vue-i18n'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Autocomplete from './components/lib/Autocomplete'
import InputValidation from './components/lib/InputValidation'
import '../assets/scss/style.scss'
import Notify from 'vue2-notify'
import Datepicker from 'lb-vue-datetimepicker'
import wysiwyg from "vue-wysiwyg";
import "vue-wysiwyg/dist/vueWysiwyg.css";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(fas)



Vue.use(Notify, {position: 'top-right'})
Vue.$notify.notice = function(data)
{
	if(typeof data == 'object' && typeof data.error != 'undefiend')
	{
		var notice = "";
		if(typeof data.fieldErrors != 'undefined')
		{

			for(var key in data.fieldErrors)
			{
				notice += data.fieldErrors[key]+"<br>";
			}
		}
		else notice = data.notice;

		if(data.error) Vue.$notify.error(notice, {mode: 'html'});
		else Vue.$notify.info(notice, {mode: 'html'});
	}
	else Vue.$notify.info(data, {mode: 'html'});
}

Vue.use(BootstrapVue);
Vue.use(VueI18n)
Vue.use(wysiwyg, {
	/*
	image: {
		uploadURL: "/api/myEndpoint",
		dropzoneOptions: {}
	},
	*/
	maxHeight: "500px",
	forcePlainTextOnPaste: true
});

Vue.component('datepicker', Datepicker)
Vue.component('icon', FontAwesomeIcon)
Vue.component('autocomplete', Autocomplete)
Vue.component('inputvalidation', InputValidation)

Vue.config.productionTip = false
const i18n = new VueI18n({});

new Vue({
	router,
	store,
	i18n,
	render: h => h(App)
}).$mount('#app')
