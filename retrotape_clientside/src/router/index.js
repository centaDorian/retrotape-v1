import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '@/containers/Full'

// Views
import Login from '@/views/admin/Login'
import Dashboard from '@/views/admin/Dashboard'
import Acl from '@/views/admin/Acl'
import Translator from '@/views/admin/Translator'
import Languages from '@/views/admin/Languages'
import SiteProps from '@/views/admin/SiteProps'
import EmailTemplates from '@/views/admin/EmailTemplates'
import UserCompanies from '@/views/admin/UserCompanies'
import Users from '@/views/admin/Users'
//import UsersList from '@/views/admin/UsersList'
import FilmList from '@/views/admin/FilmList'
import Genres from '@/views/admin/Genres'
import Rent from '@/views/admin/Rent'
import LateFee from '@/views/admin/LateFee'
import Generator from '@/views/admin/Generator'
import WikiCategory from '@/views/admin/WikiCategory'
import WikiTemplates from '@/views/admin/WikiTemplates'
import WikiNew from '@/views/admin/WikiNew'
import Wikis from '@/views/admin/Wikis'
//GENERATORLASTITEM

Vue.use(Router)

export default new Router({
	//mode: 'hash',
	//linkActiveClass: 'open active',
	//scrollBehavior: () => ({ y: 0 }),
	routes:
	[
		{
      path: '/admin/dashboard',
      name: 'HOME',
      component: Full,
      children: [
        {
          path: 'dashboard',
          name: 'DASHBOARD',
          component: Dashboard
        }
			]
    },
    {
      path: '/admin/sitecontent',
      name: 'SITECONTENT',
      component: Full,
      children: [
          {
            path: 'wikicategories',
            name: 'WIKICATEGORIES',
            component: WikiCategory
          },
          {
            path: 'wikitemplates',
            name: 'WIKITEMPLATES',
            component: WikiTemplates
          },
          {
            path: 'newwiki',
            name: 'NEWWIKI',
            component: WikiNew
          },
          {
            path: ':wikiId/editwiki',
            name: 'EDITWIKI',
            component: WikiNew
          },
          {
            path: 'wikis',
            name: 'WIKIS',
            component: Wikis
          }
			]
		},
		{
      path: '/admin/system',
      name: 'SYSTEM',
      component: Full,
      children: [
        {
          path: 'acl',
          name: 'ACL',
          component: Acl
        },
				{
          path: 'translator',
          name: 'TRANSLATOR',
          component: Translator
        },
				{
          path: 'languages',
          name: 'LANGUAGES',
          component: Languages
        },
        {
          path: 'users',
          name: 'USERS',
          component: Users
        },
        {
          path: 'films/filmList',
          name: 'FILMLIST',
          component: FilmList
        },
        {
          path: 'genres/genreList',
          name: 'GENRELIST',
          component: Genres
        },
        {
          path: 'rents/rent',
          name: 'RENT',
          component: Rent
        },
        {
          path: 'fees/fee',
          name: 'FEE',
          component: LateFee
        },
				{
          path: 'siteprops',
          name: 'SITEPROPS',
          component: SiteProps
        },
				{
          path: 'emailtemplates',
          name: 'EMAILTEMPLATES',
          component: EmailTemplates
        },
				{
          path: 'usercompanies',
          name: 'USERCOMPANIES',
          component: UserCompanies
        },
        // {
        // path: 'users/usersList',
        // name: 'USERLIST',
        // component: UsersList
        // },
				{
          path: 'generator',
          name: 'GENERATOR',
          component: Generator
        }//GENERATORINSERT
			]
		},
		{
      path: '/admin',
      name: 'LOGIN',
      component: Login
    }
	]
})
